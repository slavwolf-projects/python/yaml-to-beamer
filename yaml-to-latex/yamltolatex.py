import yaml
from helpers import ordered_load
from jinja2 import Template
import subprocess
import os
import shutil

presentation = "presentation.yaml"

with open(presentation) as stream:
    p_data = ordered_load(stream, yaml.SafeLoader)

def generate_tex(template, output):
    with open("templates/" + template + ".j2") as template_file:
        template = Template(template_file.read())

    with open(output + ".tex", "w") as generated:
        generated.write(template.render(pre=p_data))

def compile_tex():
    subprocess.call(["rubber", "-d", "full"])
    subprocess.call(["rubber", "--clean", "full"])
    shutil.move(os.getcwd() + "/full.pdf", os.getcwd() + "/output/presentation.pdf")
    shutil.move(os.getcwd() + "/generated.tex", os.getcwd() + "/output/presentation.tex")

def cleanup():
    files = os.listdir(os.getcwd())

    for item in files:
        if item.endswith(".tex"):
            os.remove(os.path.join(os.getcwd(), item))

if __name__ == "__main__":
    generate_tex("content", "generated")
    generate_tex("full", "full")
    compile_tex()
    cleanup()