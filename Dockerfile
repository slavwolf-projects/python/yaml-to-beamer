FROM python:3.7

RUN apt update && \
    apt install rubber texlive-latex-recommended texlive-latex-extra lmodern texlive-fonts-recommended -y --no-install-recommends && \
    rm -rf /var/lib/apt/lists/* && \
    pip install PyYAML jinja2

COPY yaml-to-latex /app

CMD [ "python", "/app/yamltolatex.py"]